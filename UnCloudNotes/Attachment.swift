//
//  UnCloudNotes.swift
//  UnCloudNotes
//
//  Created by Brandon Levasseur on 1/9/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Attachment: NSManagedObject {

    @NSManaged var dateCreated: NSDate
    @NSManaged var note: Note

}
