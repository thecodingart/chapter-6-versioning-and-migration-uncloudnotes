//
//  UnCloudNotes.swift
//  UnCloudNotes
//
//  Created by Brandon Levasseur on 1/14/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import CoreData

class ImageAttachment: Attachment {

    @NSManaged var caption: String
    @NSManaged var height: NSNumber
    @NSManaged var image: UIImage?
    @NSManaged var width: NSNumber

}
