//
//  AttachmentToImageAttachmentMigrationPolicyV3toV4.swift
//  UnCloudNotes
//
//  Created by Brandon Levasseur on 1/13/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import CoreData

class AttachmentToImageAttachmentMigrationPolicyV3toV4: NSEntityMigrationPolicy {
    override func createDestinationInstancesForSourceInstance(sInstance: NSManagedObject, entityMapping mapping: NSEntityMapping, manager: NSMigrationManager, error: NSErrorPointer) -> Bool {
        let newAttachment = NSEntityDescription.insertNewObjectForEntityForName("ImageAttachment", inManagedObjectContext: manager.destinationContext) as NSManagedObject
        
        for propertyMapping in mapping.attributeMappings as [NSPropertyMapping]! {
            let destinationName = propertyMapping.name!
            if let valueExpression = propertyMapping.valueExpression {
                let context: NSMutableDictionary = ["source": sInstance]
                let destinationValue: AnyObject = valueExpression.expressionValueWithObject(sInstance, context: context)
                newAttachment.setValue(destinationValue, forKey: destinationName)
            }
        }
        
        if let image = sInstance.valueForKey("image") as? UIImage {
            newAttachment.setValue(image.size.width, forKey: "width")
            newAttachment.setValue(image.size.height, forKey: "height")
        }
        
        let body = sInstance.valueForKeyPath("note.body") as NSString
        newAttachment.setValue(body.substringToIndex(80), forKey: "caption")
        
        manager.associateSourceInstance(sInstance, withDestinationInstance: newAttachment, forEntityMapping: mapping)
        
        return true
    }
}
